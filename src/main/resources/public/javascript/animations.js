
function slideToPlanner()
{
    $(document).ready(function()
    {
        $("#tracker").animate(
        {
            left: '0%'
        });

        $("#tracker").fadeIn();
        $("#planner").fadeIn();
        $('#contact').fadeOut();
        $('#privacy').fadeOut();
        $('#about').fadeOut();
    });
}

function slideToTracker()
{
    $(document).ready(function()
    {
        $("#tracker").animate(
        {
            left: '25%'
        });

        $("#tracker").fadeIn();
        $("#planner").fadeOut();
        $('#contact').fadeOut();
        $('#privacy').fadeOut();
        $('#about').fadeOut();
    });
}

function slideToContact()
{
    $(document).ready(function()
    {
        $('#contact').fadeIn();
        $("#tracker").fadeOut();
        $("#planner").fadeOut();
        $('#privacy').fadeOut();
        $('#about').fadeOut();
    });
}

function slideToPrivacy()
{
    $(document).ready(function()
    {
        $('#privacy').fadeIn();
        $('#contact').fadeOut();
        $("#tracker").fadeOut();
        $("#planner").fadeOut();
        $('#about').fadeOut();
    });
}

function slideToAbout()
{
    $(document).ready(function()
    {
        $('#about').fadeIn();
        $('#privacy').fadeOut();
        $('#contact').fadeOut();
        $("#tracker").fadeOut();
        $("#planner").fadeOut();
    });
}

function changeActiveNavButton(id)
{
    var newId = "#" + id;
    $(document).ready(function()
    {
        $("navbar").ready(function()
        {
            $("a").each(function()
            {
                $(this).removeClass("active");
            });

            $(newId).addClass("active");
        });
    });
}
