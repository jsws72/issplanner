var http = new XMLHttpRequest();

var marker;
var cityMarker;
var map;
var circle;

function start()
{
    hideLoader();
    initMap();
    getISSOpenNotify();
    getISSData();
}

function requestCity()
{
    showLoader();
    $('#isspass').text("");

    var cityName = document.getElementById("cityNameInput").value

    if(cityName != "")
    {
        getForwardGeocode(cityName, showPassTimeAndWeather);
        getWeatherPost(cityName);
    }
    else
    {
        getUserLocation(showPassTimeAndWeatherGeolocation);
    }
}

function showPassTimeAndWeather(data)
{
    if(data.hasOwnProperty('error'))
    {
        hideLoader();

        const description = data['error']['description'];
        $('#isspass').html("<p>An error occurred when calling the API (geocode.xyz).</p><p>" + description + "</p>");
    }
    else
    {
        var lat = data['latt'];
        var lon = data['longt'];

        getPassTimes(lat, lon);
        showRequestedCityOnMap(lat, lon);
    }
}

function showPassTimeAndWeatherGeolocation(position)
{
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    getPassTimes(lat, lon);
    getWeatherPost(lat, lon);
    showRequestedCityOnMap(lat, lon);
}

function showRequestedCityOnMap(lat, lon)
{
    cityMarker.setLatLng([lat, lon]);
}

function initMap()
{
    map = L.map('map').setView([0.0, 0.0], 3);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZHJhbW9uIiwiYSI6ImNrY3J6ZG0wNDBmZWgzMG9lOGh2OTJ6bWkifQ.WDcRyQwEF0YhOZdJNdgfCQ'
    }).addTo(map);

    var myIcon = L.icon({
        iconUrl: 'img/ISS_Icon.png',
        iconSize: [32, 32],
        iconAnchor: [16, 16],
        popupAnchor: [-3, -76],
    });
    cityMarker = L.marker([0.0, 0.0]).addTo(map);
    marker = L.marker([0.0, 0.0], {icon: myIcon}).addTo(map);
    circle = L.circle([0.0, 0.0], {
        color: 'green',
        fillOpacity: 0.25,
        radius: 1500000
    }).addTo(map);
}

function moveISSOpenNotify(data)
{
    var lat = data['iss_position']['latitude'];
    var lon = data['iss_position']['longitude'];

    // See leaflet docs for setting up icons and map layers
    // The update to the map is done here:
    marker.setLatLng([lat, lon]);
    circle.setLatLng([lat, lon]);
    map.panTo([lat, lon], animate=true);

    $('#coordinates').html("<b>Latitude:</b> " + lat + " | <b>Longitude:</b> " + lon);
}

function displayISSData(data)
{
    /* incorrect data
    var lat = data['latitude'];
    var lon = data['longitude'];
    */
    var alt = data['altitude'];
    var vel = Math.round(data['velocity']);

    var altText = "<b>Altitude:</b> " + parseInt(alt) + "km";
    var velocityText = "<b>Velocity:</b> " + vel + "km/h";
    $('#issData').html(altText + " | " + velocityText);
}

function displayPassTimes(data)
{
    if(data['message'] == "success")
    {
        hideLoader();
        $('#isspass').text("");
        data['response'].forEach(function (d)
        {
            var date = new Date(d['risetime']*1000);
            $('#isspass').append('<li>' + date.toLocaleTimeString('de-DE') + '</li>');
        });
    }
    else
    {
        hideLoader();
        $('#isspass').html("<p>An error occurred when calling the API.</p><p>Maybe there is no data available for your requested location.</p>");
    }
}

// this function is deprecated!
function getCoordinatesByCityName(cityName, callback)
{
    console.warn('This function is deprecated (getCoordinatesByCityName)');
    $.getJSON('https://geocode.xyz/'+ cityName + '?json=1&auth=958857070837367757819x6854', function(data)
    {
        var lat = data['latt'];
        var lon = data['longt'];
        //console.log([lat, lon]);

        callback(lat, lon);
    }).fail(function(jqXHR, textStatus, errorThrown)
    {
        hideLoader();
        $('#isspass').html("<p>An error occurred when calling the API (geocode.xyz).</p><p>Please retry in a few seconds again.</p>");
    });
}

function displayWeather(data)
{
    if(data['cod'] == 200)
    {
        var icon = data['weather'][0].icon;
        var description = data['weather'][0].description;

        $('#weatherIcon').html('<img src="http://openweathermap.org/img/wn/' + icon + '@2x.png" alt="weather icon">');
        $('#weatherDescription').html('<p>' + description + '</p>');
    }
    else
    {
        $('#weatherIcon').html('<p></p>');
        $('#weatherDescription').html('<p>No weather available</p>');
    }
}

function showLoader()
{
    document.getElementById("loader").style.display = "block";
}

function hideLoader()
{
    document.getElementById("loader").style.display = "none";
}

function getUserLocation(callback)
{
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(callback);
    }
    else
    {
        console.log("Geolocation is not supported by this browser.");
    }
}