async function getWeatherPost(lat, lon)
{
    const url = '/weather/' + lat + "/" + lon;
    const response = await fetch(url);
    const json = await response.json();

    displayWeather(json);
}

async function getWeatherPost(cityName)
{
    const url = '/weather/' + cityName;
    const response = await fetch(url);
    const json = await response.json();

    displayWeather(json);
}

async function getPassTimes(lat, lon)
{
    const url = '/passTimes/' + lat + "/" + lon;
    const response = await fetch(url);
    const json = await response.json();

    displayPassTimes(json);
}

async function getISSData()
{
    const url = '/issData';
    const response = await fetch(url);
    const json = await response.json();

    displayISSData(json);
    setTimeout(getISSData, 5000);
}

async function getISSOpenNotify()
{
    const url = '/issPosition';
    const response = await fetch(url);
    const json = await response.json();

    moveISSOpenNotify(json);
    setTimeout(getISSOpenNotify, 5000);
}

async function getForwardGeocode(cityName, callback)
{
    const url = '/forwardGeocode/' + cityName;
    const response = await fetch(url);
    const json = await response.json();

    callback(json);
}