var http = require('http');
    fs = require('fs');

function readStaticFileToRes(res, path, contentType, responseCode)
{
    if(!responseCode)
    {
        responseCode = 200;
    }

    fs.readFile(__dirname + "/" + path, 'utf8', function(err, data)
    {
        if(err)
        {
            res.writeHead(500, {'Content-Type': 'text/plain'});
            res.end('Internal error');
        }
        else
        {
            res.writeHead(responseCode, {'Content-Type': contentType});
            res.end(data);
        }
    });
}

var server = http.createServer(function(req, res)
{
    var path = req.url.replace(/\/?(?:\?.*)?$/, '');
    console.log('request was made: ' + path);

    switch(path)
    {
        case '':
            readStaticFileToRes(res, 'public/index.html', 'text/html');
            break;
        case '/img/earthRender.png':
            readStaticFileToRes(res, 'public/img/earthRender.png', 'image/png');
            break;
        case '/img/ISS_Icon.png':
            readStaticFileToRes(res, 'public/img/ISS_Icon.png', 'image/png');
            break;
        case '/img/ISS_Icon.png':
            readStaticFileToRes(res, 'public/img/ISS_Icon.png', 'image/png');
            break;
        case '/javascript/main.js':
            readStaticFileToRes(res, 'public/javascript/main.js', 'text/js');
            break;
        case '/javascript/animations.js':
            readStaticFileToRes(res, 'public/javascript/animations.js', 'text/js');
            break;
        case '/javascript/smoothScroll.js':
            readStaticFileToRes(res, 'public/javascript/smoothScroll.js', 'text/js');
            break;
        case '/css/style.css':
            readStaticFileToRes(res, 'public//css/style.css', 'text/css');
            break;
        case '/favicon.ico':
            readStaticFileToRes(res, 'public/img/ISS_Icon.png', 'image/png', 204);
            break;
        default:
            console.log('default');
    }
});

server.listen(7070);
console.log('Server started');