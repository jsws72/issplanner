const express = require('express');
const fetch = require('node-fetch');
require('dotenv').config();
const app = express();

app.use(express.static(__dirname + '/public'));



app.get('/weather/:lat/:lon', async (request, response) =>
{
    const lat = request.params['lat']
    const lon = request.params['lon']

    const apiKey = process.env.API_KEY_WEATHER;
    const apiURL = 'http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&appid=' + apiKey;
    const fetch_response = await fetch(apiURL);
    const json = await fetch_response.json();

    response.json(json);
});

app.get('/weather/:cityName', async (request, response) =>
{
    const cityName = request.params['cityName']

    const apiKey = process.env.API_KEY_WEATHER;
    const apiURL = 'http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=' + apiKey;
    const fetch_response = await fetch(apiURL);
    const json = await fetch_response.json();

    response.json(json);
});

app.get('/passTimes/:lat/:lon', async (request, response) =>
{
    const lat = request.params['lat']
    const lon = request.params['lon']

    const apiURL = 'http://api.open-notify.org/iss-pass.json?lat=' + lat + '&lon=' + lon + '&alt=20&n=5';
    const fetch_response = await fetch(apiURL).catch(function(error) { console.log('Error') });
    const json = await fetch_response.json();

    response.json(json);
});

app.get('/issData', async (request, response) =>
{
    const apiURL = 'https://api.wheretheiss.at/v1/satellites/25544';
    const fetch_response = await fetch(apiURL);
    const json = await fetch_response.json();

    response.json(json);
});

app.get('/issPosition', async (request, response) =>
{
    const apiURL = 'http://api.open-notify.org/iss-now.json?';
    const fetch_response = await fetch(apiURL);
    const json = await fetch_response.json();

    response.json(json);
});

app.get('/forwardGeocode/:cityName', async (request, response) =>
{
    const cityName = request.params['cityName']

    const apiKey = process.env.API_KEY_GEOCODE;
    const apiURL = 'https://geocode.xyz/'+ cityName + '?json=1&auth=' + apiKey;
    const fetch_response = await fetch(apiURL);//.catch(error => { console.log('geocode.xyz request threw an error: ' + error); });
    const json = await fetch_response.json();

    response.json(json);
});

var server = app.listen(process.env.PORT || 7070, function()
{
    console.log('Node server is running on port ' + server.address().port + '...');
});