# About
You can find the Website [here](http://issplanner.herokuapp.com/)

This website is used to localize and plan a sighting of the ISS.

With the planner you can specify the city in which you want to observe the ISS. If you leave the input field empty, your current location will be used.
For each requested city the current weather is shown.

A marker will show your requested city location or the Null Island (0°N, 0°E) if no city was requested or an error is listed.

The best conditions for sighting the ISS are at night, right after sunset or before sunrise, and with clear skies.

### Troubleshooting
**Note:** the requested city names have to be in english!

If the pass times or weather are not displayed or are not loaded, an error occurred when calling the API.

Maybe there is no data available for your requested location or the rate limit for requests is reached.
Because free APIs are used there is a poll rate limit for some APIs. For example geocode.xyz has a rate limit of 10 requests per second.
Also open-notify.org has a maximum poll rate of 1Hz, to update the location of the ISS so the Website updates the location each 5s.

### Used APIs
* api.mapbox.com (OpenStreetMap)
* openweathermap.com (weather)
* geocode.xyz (Forward geocoding -> city name to coordinates)
* api.open-notify.org (ISS location & pass times)
* api.wheretheiss.at (ISS altitude & velocity data)

# Installation guide
### Requirements
* [Node.js](https://nodejs.org/en/download/) and
* [Git](https://git-scm.com/) installed

### Guide
* Clone this project
* Navigate to *src/main/resources*
* Type following commands to install dependencies:
```bash
npm install express --save
```
```bash
npm install node-fetch
```
```bash
npm install dotenv
```
* Go to [geocode.xyz](https://geocode.xyz/new_account) and [openweathermap.org](https://home.openweathermap.org/users/sign_in) and register to create an API-Key
* Open the *.env* file and replace *{YOUR_API_KEY_FROM_openweathermap.org}* and *{YOUR_API_KEY_FROM_geocode.xyz}* with your keys
* Run the server with (note: from root directory of the project):
```bash
npm start
```

# Contact
If you have any trouble or questions, write me at
* jacob.schwass@mni.thm.de